/*
        This is based on https://github.com/bespoken/super-simple-google-action
        updated some stuff for dialogflow 2.0 API
        added code to interact with postgresql-database (e.g. running in docker-container)
 */
const bst = require("bespoken-tools")
const { Pool, Client } = require('pg')

const projectId = ''


  

const simpleFunction = function (request, response) {

    //
    //      HANDLE Intent
    //
    const intent = request.body.queryResult.intent;
    const params = request.body.queryResult.parameters
    const session = request.body.session

    console.log("Intent: " + intent.displayName);

    const responseJSON = {
        fulfillmentText: '',
        fulfillmentMessages: null,
        payload: null,
        outputContexts: null,
        followupEventInput: null,
        source: "adorsys_cui_lab"
    }

    let answer = ''

    if (intent.displayName === "<One of your intent-names here>") {
        console.log("Intent: ....");
        responseJSON.fulfillmentText = 'An answer for dialogflow'
        response.json(responseJSON)
    } else if (intent.displayName === "<Another of your intent-names here>") {
        console.log("Intent: ....");
        responseJSON.fulfillmentText = 'An answer for dialogflow'
        response.json(responseJSON)
    } else if (intent.displayName === "SendMoneyIntent") {
        console.log("MY Intent: " + intent.displayName);
        //
        // Connect to database
        //
        const client = connectClient()

        //
        //  Do something with database
        //
        createTable(client)
            .then( () => {
                return lookFor(client, '')
            })
            .then(res => {
                if (res.rowCount == 0) {
                    //write something to -> answer
                } else {
                    //write something else to -> answer
                }
            })
            .catch(e => {
                console.error(e.stack)
            })
            .finally(() => {
                disconnect(client)
                responseJSON.fulfillmentText = answer
                response.json(responseJSON)
            }) 

    }
};

exports.simpleFunction = bst.Logless.capture("<YOUR_SECRET_KEY>", simpleFunction);


//
//      Helpers to database-related operations
//

function connectClient() {
    const client = new Client({
        user: 'postgres',
        host: '0.0.0.0',
        database: '<database-name>',
        password: '<password - default for postgres is: postgres>',
        port: 5432
      })

      client.connect()

      return client
}

function disconnect(client) {
    return client.end()
}

function createTable(client) {
    const queryText = 'CREATE TABLE IF NOT EXISTS yourTableName(id SERIAL PRIMARY KEY, col1 VARCHAR (50) NOT NULL, col2 VARCHAR (50) NOT NULL, col3 VARCHAR (34) NOT NULL);'

    return client.query(queryText)
}

// data is of form: ['val1', 'val2', 'val3']
function addUser(client, data) {
    const query = {
        text: 'INSERT INTO yourTableName(col1, col2, col3) VALUES($1, $2, $3)',
        values: data,
      }

      return client.query(query)
}

function lookFor(client, value) {
    const query = {
        text: 'SELECT * FROM yourTableName WHERE lower(col1) = lower($1)',
        values: [value],
      }

      return client.query(query)
}

//
//      Helpers for dialogfow context handling 
//      (e.g. setting output-context to trigger intent with respective input-context)
//

function contextName(session, targetContext) {
    return session + '/contexts/' + targetContext.toLowerCase()
}

function transactionConformationContext(session) {
    return {
        name: contextName(session, '<inputContextName>'),
        lifespanCount: 1,
        parameters: {
            
        }
    }
}

//
//      Helpers to handle voice-ui all lower-case input style
//

function capitaliseName(name) {
    if (name.includes(' ')) {
        return name.split(' ').forEach(e => capitalizeFirstLetter(e)).join(' ')
    } else if (name.includes('-')) {
        return name.split('-').forEach(e => capitalizeFirstLetter(e)).join('-')
    } else {
        return capitalizeFirstLetter(name)
    }
}

//https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

