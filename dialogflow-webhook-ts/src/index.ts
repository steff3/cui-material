import { BaseIntentHandler } from './BaseIntentHandler'
import { PersistenceHandler } from './PersistenceHandler'

import { Logless } from 'bespoken-tools'

const persistenceHandler: PersistenceHandler = new PersistenceHandler()

const handlers: BaseIntentHandler[] = []

const simpleFunction = function (request: any, response: any) {
    let matchingHandler: BaseIntentHandler = null
    handlers.forEach(e => { if (e.canHandle(request)) { matchingHandler = e } })

    if (matchingHandler != null) {
        matchingHandler.handleIntent()
            .then(res => { 
                response.json(res) 
            })
    }

}

exports.simpleFunction = Logless.capture("<YOUR_SECRET_KEY>", simpleFunction);
