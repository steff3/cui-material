
export abstract class BaseIntentHandler {
    protected utterance: string
    protected intent: any
    protected params: any
    private session: any

    protected isCompleted: boolean = true
    protected hasFollowUp: boolean = false

    protected responseJson: {
        fulfillmentText: string
        fulfillmentMessages: any
        payload: any
        outputContexts: any
        followupEventInput: any
        source: string
    }

    constructor() {
        
    }

    handleIntent(): Promise<any> {
        return new Promise<any>((res) => { 
            this.initialiseResponse()
            res(null)
        })
    }

    canHandle(request: any): boolean {
        this.utterance = request.body.queryResult.queryText
        this.intent = request.body.queryResult.intent
        this.params = request.body.queryResult.parameters
        this.session = request.body.session

        return false
    }

    //
    //      Response Object
    //
    protected initialiseResponse() {
        this.responseJson = {
            fulfillmentText: '',
            fulfillmentMessages: null,
            payload: null,
            outputContexts: null,
            followupEventInput: null,
            source: "adorsys_cui"
        }
    }

    //
    //      Helpers for dialogfow context handling 
    //      (e.g. setting output-context to trigger intent with respective input-context)
    //
    private contextName(targetContext: string): string {
        return this.session + '/contexts/' + targetContext.toLowerCase()
    }

    protected constructContext(name: string, params: any = null, lifespan: number = 1): any {
        let context: any

        if (params != null) {
            context = {
                name: this.contextName(name),
                lifespanCount: lifespan,
                parameters: params
            }
        } else {
            context = {
                name: this.contextName(name),
                lifespanCount: lifespan
            }
        }
        
        return context
    }
}