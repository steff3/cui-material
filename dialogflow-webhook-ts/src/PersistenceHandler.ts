import { Client } from 'pg'



export class PersistenceHandler {


    //
    //      Data handling helper methods
    //  

    public capitaliseName(name: string): string {
        if (name.includes(' ')) {
            const parts: string[] = []
            name.split(' ').forEach(e => { parts.push(this.capitalizeFirstLetter(e)) })
            return parts.join(' ')
        } else if (name.includes('-')) {
            const parts: string[] = []
            name.split('-').forEach(e => { parts.push(this.capitalizeFirstLetter(e)) })
            return parts.join('-')
        } else {
            return this.capitalizeFirstLetter(name)
        }
    }
    
    //https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
    private capitalizeFirstLetter(varString: string): string {
        return varString.charAt(0).toUpperCase() + varString.slice(1);
    }

    //
    //      Database related methods
    //  

    private connectClient(): any {
        const client = new Client({
            user: 'postgres',
            host: '0.0.0.0',
            database: 'agentdb',
            password: 'postgres',
            port: 5432
          })
    
          client.connect()
    
          return client
    }

    private disconnect(client: any): Promise<any> {
        return client.end()
    }

    private createTable(client: any): Promise<any> {
        const queryText = 'CREATE TABLE IF NOT EXISTS table_name(item_id SERIAL PRIMARY KEY, val_1 VARCHAR (50) NOT NULL, val_2 VARCHAR (50) NOT NULL, val_3 VARCHAR (34) NOT NULL);'
    
        return client.query(queryText)
    }

    private addItem(client: any, data: string[]): Promise<any> {
        const query = {
            text: 'INSERT INTO table_name(val_1, val_2, val_3) VALUES($1, $2, $3)',
            values: data,
          }
    
          return client.query(query)
    }

    private findByVal1(client: any, value: string): Promise<any> {
        const query = {
            text: 'SELECT * FROM table_name WHERE lower(val_1) = lower($1)',
            values: [value],
          }
    
          return client.query(query)
    }
}